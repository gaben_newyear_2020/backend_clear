import json
import pika

from newyear.settings.base import AMQP_URL


async def rabbit_send(message: dict):
    parameters = pika.URLParameters(AMQP_URL)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()

    queue = "vk_sendler_anon"

    channel.queue_declare(queue=queue)

    channel.basic_publish(
        exchange='',
        routing_key=queue,
        body=json.dumps(message, ensure_ascii=False, default=str)
    )
    connection.close()
