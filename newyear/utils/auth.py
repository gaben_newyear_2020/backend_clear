import functools
import json

import vk
from base64 import b64encode
from collections import OrderedDict
from hashlib import sha256
from hmac import HMAC
from urllib.parse import urlparse, parse_qsl, urlencode

from newyear.models.users import User
from newyear.settings.conf import *
from .errors import ForbiddenError
from newyear.app import redis_client


def is_valid(*, query: dict, secret: str) -> bool:
    """Check VK Apps signature"""
    vk_subset = OrderedDict(sorted(x for x in query.items() if x[0][:3] == "vk_"))
    hash_code = b64encode(HMAC(secret.encode(), urlencode(vk_subset, doseq=True).encode(), sha256).digest())
    decoded_hash_code = hash_code.decode('utf-8')[:-1].replace('+', '-').replace('/', '_')
    return query["sign"] == decoded_hash_code


def validate(sign: str) -> tuple:
    """
    Валидация подписи
    :param sign: подпись с аудентификации
    :return:
        [0] -> error True or False
        [1] -> user(vk_id): int
        [2] -> query_params: dict
    """
    try:
        query_params = dict(parse_qsl(urlparse(sign).query, keep_blank_values=True))
        print(CLIENT_SECRET)
        if is_valid(query=query_params, secret=CLIENT_SECRET):
            return False, int(query_params.get('vk_user_id')), query_params
    except:
        pass
    return True, 0, dict()


async def get_user(vk_user_id: int):
    user = await User.get_or_create_by_vk_id(vk_user_id)
    return user.get_id()


def auth():
    def decorator(func):
        @functools.wraps(func)
        async def wrapper(*args, **kwargs):
            user: str = kwargs.get("sign", None)
            if user is None:
                raise ForbiddenError(data={"details": "No user fields into params"})
            if RUN_TYPE != "LOCAL":
                err, vk_user_id, _ = validate(user)
                if err:
                    raise ForbiddenError(data={"details": "Sign no valid"})
                user_id = await get_user(vk_user_id)
            else:
                user_id = await get_user(user)
            kwargs['x_user_id'] = int(user_id)
            result = await func(*args, **kwargs)
            return result

        return wrapper

    return decorator


async def info_users_in_vk(users_ids, fields) -> list:
    user_ids_str = ",".join(map(str, sorted(list(users_ids))))
    fields_str = ",".join(map(str, sorted(list(fields))))
    r_name, r_key = "vk_user", f'{user_ids_str}|{fields_str}'
    cache = redis_client.hget(r_name, r_key)
    if cache:
        return json.loads(cache)

    session = vk.Session(access_token=TOKEN_SERVICE)
    vk_api = vk.API(session)
    resp = vk_api.users.get(
        v="5.124",
        user_ids=user_ids_str,
        fields=fields_str,
        lang=0,
    )
    r_value = json.dumps(resp, ensure_ascii=False, default=str)
    redis_client.hset(r_name, r_key, r_value)
    redis_client.expire(name=r_name, time=18000)

    return resp
