import fastapi_jsonrpc as jsonrpc
from pydantic import BaseModel


class ForbiddenError(jsonrpc.BaseError):
    CODE = 403
    MESSAGE = 'Forbidden Error'

    class DataModel(BaseModel):
        details: str


class FieldNoValid(jsonrpc.BaseError):
    CODE = 500
    MESSAGE = 'Field is not valid'

    class DataModel(BaseModel):
        details: str
