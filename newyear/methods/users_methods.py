import typing as t
from newyear.app import api_v1
from newyear.utils.auth import auth, info_users_in_vk, retry_connect
from newyear.models.users import User
from newyear.models.postcard import PostcardData
from newyear.settings.conf import db_manager


@api_v1.method()
@retry_connect()
@auth()
async def user_open_app(
        sign: str,
        x_user_id: int = -1
) -> dict:
    """
    Функция, которая отдаёт первичные данные для входа в приложение
    Срабатывает один раз при входе в приложение
    :param: sign - используется при авторизации пользователя (токен от VK)
    :param: x_user_id - после авторизации прописывается в функцию и определяет идентификатор пользователя в системе
    :return:
        unread: Кол-во не прочитанных открыток для пользователя
    """
    user = await User.get_object(x_user_id)
    count_unread_message = await db_manager.count(
        PostcardData.select(PostcardData.id).where(
            (PostcardData.to_user == user) &
            (PostcardData.is_view == False)
        )
    )
    resp = await user.get_full_info()
    resp.update(dict(unread=count_unread_message))
    return resp


async def users_get_info_in_vk(
        sign: str,
        user_ids: t.List[int],
        x_user_id: int = -1
) -> t.List[t.Dict]:
    """
    Функция возвращает информацию о пользователях с fields полями по умолчанию
    :param user_ids: Выбранные пользователи users_ids
    :param sign: Подпись человека
    :param x_user_id: id человека в системе
    :return:
    """
    return await info_users_in_vk(
        user_ids,
        ["photo_200"]
    )
