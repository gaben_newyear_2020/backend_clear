from newyear.utils.djantimat.helpers import RegexpProc


def validate_swearing(text: str) -> bool:
    return not RegexpProc.test(text)


def auto_recreate_text(text):
    import requests
    import json
    url = "https://pelevin.gpt.dobro.ai/generate/"

    headers = {
        'Connection': 'keep-alive',
        'Pragma': 'no-cache',
        'Cache-Control': 'no-cache',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Mobile Safari/537.36',
        'Content-Type': 'text/plain;charset=UTF-8',
        'Accept': '*/*',
        'Origin': 'https://porfirevich.ru',
        'Sec-Fetch-Site': 'cross-site',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Dest': 'empty',
        'Accept-Language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7'
    }
    try:
        response = requests.request("POST", url, headers=headers,
                                    data=json.dumps(dict(prompt=text))
                                    )
        return json.loads(response.text)['replies']
    except:
        return None
