from newyear.app import api_v1


@api_v1.method()
async def echo() -> dict:
    return dict(
        status="ok",
        text="api is start"
    )
