import datetime
import json
import typing as t
from fastapi import BackgroundTasks
from newyear.utils.auth import info_users_in_vk, retry_connect
from newyear.app import api_v1, redis_client
from newyear.models.postcard import PostcardDesign, PostcardData, PostcardGreeting
from newyear.models.users import User
from newyear.utils.auth import auth
from newyear.utils.errors import FieldNoValid
from .postcard_methods_utlis import validate_swearing, auto_recreate_text
from newyear.utils.rabbit_send import rabbit_send


async def postcard_text_validate_logic(text: str):
    return validate_swearing(text)


@api_v1.method()
@retry_connect()
async def postcard_random_greetings() -> dict:
    rows = await PostcardGreeting.get_random_greeting()
    return dict(
        count=len(rows),
        greetings=rows,
    )


@api_v1.method()
async def postcard_generate_greetings(
        text: str
) -> dict:
    """
    Автодополнение текста
    :param text: Текст, который необходимо дополнить
    :return:
        replies - массив из нескольких выриантов дополнения текста
    """
    text_array = auto_recreate_text(text)
    if text_array is not None:
        return dict(replies=text_array)
    return dict(replies=[])


@api_v1.method()
async def postcard_validate_text(
        text: str
) -> dict:
    is_valid = await postcard_text_validate_logic(text)
    return dict(is_valid=is_valid)


async def send_message_anon(
        vk_id,
        slug
):
    await rabbit_send(
        dict(
            vk_id=vk_id,
            slug=slug
        )
    )


@api_v1.method()
@retry_connect()
@auth()
async def postcard_create(
        sign: str,
        tasks: BackgroundTasks,
        design_id: int,
        text: str,
        to_vk_id: int = 0,
        mark_id: int = 0,
        anonymous: bool = False,
        x_user_id: int = -1,
) -> dict:
    """
    Создаёт сущность поздравления
    :param tasks: Отложенное выполнение задания
    :param mark_id: Идентификатор марки для отображения на фронте
    :param sign: подпись цифровая от VK
    :param design_id:
    :param text: Текст, который будет поздравительным
    :param to_vk_id: идентификатор для получателя
    :param anonymous: анонимное ли поздравление True/False
    :param x_user_id: параметр будет после аудентификации
    :return:
        slug - уникальная ссылка для открытки
    """
    design_card = await PostcardDesign.get_object(design_id)
    if design_card is None:
        raise FieldNoValid(dict(details="design_id"))
    if to_vk_id == 0:
        anonymous = False
    if anonymous:
        is_valid = await postcard_text_validate_logic(text)
        if not is_valid:
            raise FieldNoValid(dict(details="Invalid text field"))
    from_user = await User.get_object(x_user_id)
    to_user = await User.get_or_create_by_vk_id(to_vk_id)

    # Cache params
    cache_params = {
        "x_user_id": x_user_id,
        "to_vk_id": to_vk_id,
        "design_id": design_id,
        "text": text,
        "anonymous": anonymous
    }
    name, key = "main_postcard_create", json.dumps(cache_params)
    cache = redis_client.hget(name, key)
    if cache:
        return json.loads(cache)

    # End Cache params

    # Check allow send message
    error, data_error = await PostcardData.check_allow_send_from_user_to_user(
        from_user=from_user,
        to_user=to_user
    )
    if error:
        return data_error

    postcard = await PostcardData.create_and_generate_slug(
        from_user=from_user.get_id(),
        to_user=to_user.get_id(),
        anonymous=anonymous,
        text=text,
        design=design_card.get_id(),
        mark_id=abs(mark_id) % 127,
        time_send=datetime.datetime.now(),
    )
    if anonymous:
        tasks.add_task(
            func=send_message_anon,
            vk_id=to_vk_id,
            slug=postcard.slug
        )

    resp = dict(
        postcard_slug=postcard.slug,
    )
    # Set Cache into params on 10 min

    value = json.dumps(resp, ensure_ascii=False, default=str)
    redis_client.hset(name, key, value)
    redis_client.expire(name=name, time=600)
    return resp


@api_v1.method()
@retry_connect()
@auth()
async def postcard_get(
        sign: str,
        slug: str,
        x_user_id: int = -1
) -> dict:
    """
    Получение открытки по slug
    :param slug: Доступ к открытке
    :param sign:
    :param x_user_id:
    :return:
        from_user_id - идентификатор пользователя на платформе VK
        from_user - профиль пользователя:
            first_name,
            last_name,
            photo_200,
            ...
        text - текст поздравления
        design:
            id - уникальный идентификатор дизайна в системе
            url - ссылка на отображение картинки дизайна
        slug - уникальная строка для получения поздравления
        is_view - просмотрена ли открытка ранее
        mark_id - число, которое является идентификатором марки для отображения на конверте
    """
    is_err, info = await PostcardData.get_or_none_by_slug_and_to_user_id(
        x_user_id=x_user_id,
        slug=slug,
    )
    if is_err:
        raise FieldNoValid(info)

    info['from_user'] = None
    if info.get("from_user_id") is not None:
        user_profile = await info_users_in_vk([info.get("from_user_id")], ["photo_200"])
        info['from_user'] = user_profile[0] if len(user_profile) == 1 else None

    return info


@api_v1.method()
@retry_connect()
@auth()
async def postcard_my(
        sign: str,
        x_user_id: int = -1,
) -> dict:
    """
    Получить все открытки, принадлежащие человеку
    :param sign:
    :param x_user_id:
    :return:
    """
    postcards = await PostcardData.get_all_by_x_user_id(x_user_id)
    user_vk_ids = set([x['from_user_id'] for x in postcards if x['from_user_id'] is not None])
    user_profiles = dict((
        (int(x['id']), x) for x in await info_users_in_vk(user_vk_ids, ["photo_200"])
    ))

    for postcard in postcards:
        if postcard["from_user_id"] is not None:
            postcard["from_user"] = user_profiles.get(int(postcard["from_user_id"]), None)
        else:
            postcard["from_user"] = None

    return dict(
        count=len(postcards),
        postcards=postcards,
    )
