from yoyo import step

step(
    """
    CREATE TABLE `postcardgreeting` (
    `id` INT unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `text` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
    )
    """
)

step(
    """
    INSERT INTO `postcardgreeting` (`text`) VALUES
    ('Желаю гармонии, мира в душе, уюта и счастья в быту!'),
    ('Желаю, чтобы все сессии были закрыты, а экзамены сданы!'),
    ('Вечной молодости, азарта и огня в глазах!'),
    ('Желаю, чтоб омут в любимых глазах оставался бездонным!'),
    ('Новых побед и великих свершений'),
    ('Семейного и материального благополучия'),
    ('Хорошего настроения и теплоты внутри'),
    ('Солнца даже в грозу'),
    ('Неземной любви и сказки в жизни'),
    ('Лучиков света в тёмном царстве'),
    ('Приятных неожиданностей и подарков судьбы'),
    ('Душевного спокойствия и ослепляющего счастья'),
    ('Реализованных фантазий и воплощенных в жизнь идей'),
    ('Достигнутых целей и грандиозных планов'),
    ('Чем больше мечтаешь, тем дальше летишь'),
    ('Крыльев за спиной и вечного состояния полёта'),
    ('Волшебной эйфории и вдохновения'),
    ('Не сиди, сложа руки, претворяй мечты в жизнь'),
    ('Неземного везения и солнца над головой'),
    ('Чтоб неприятности обходили стороной, а удача шла прямо в руки'),
    ('Весь мир поможет тебе достичь того, чего ты хочешь, главное - не отпускай руки'),
    ('Не слушай людей, слушай свое сердце'),
    ('Пусть счастье поселится в твоём доме, а неудачи выселятся навсегда'),
    ('Пусть все задуманное непременно сбывается в этот светлый, радостный, полный чудес День!'),
    ('Пусть ваша жизнь радует вас исполнением желаний!'),
    ('Пусть счастье и удача сопутствуют во всех делах ваших, а беды и неудачи проходят мимо!'),
    ('Пусть душа всегда будет открыта чуду, добру и волшебству!'),
    ('Желаю стремительных взлётов, счастливых полётов, искренних объятий близких и большого счастья на Земле'),
    ('Пусть оправдаются все ожидания и сбудутся самые заветные мечты!'),
    ('Пусть Рождество подарит яркую надежду и крепкую веру, красивую сказку и верную любовь');
    """
)
