from yoyo import step

step(
    """
    CREATE TABLE `user` (
    `id` INT unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `vk_id` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    `snow` INT unsigned NOT NULL DEFAULT '5',
    `time_registered` DATETIME NOT NULL,
    `last_login` DATETIME DEFAULT NULL
    )
    """
)

step(
    """
    CREATE TABLE `postcarddesign` (
        `id` INT unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
        `path` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
        )
    """
)

step(
    """
    CREATE TABLE `postcarddata` (
    `id` INT unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `from_user_id` INT unsigned NOT NULL,
    `to_user_id` INT unsigned NOT NULL,
    `text` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    `slug` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    `design_id` INT unsigned NOT NULL,
    `mark_id` TINYINT unsigned NOT NULL DEFAULT '0',
    `time_send` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `is_view` BOOLEAN NOT NULL DEFAULT '0',
    FOREIGN KEY (`from_user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE,
    FOREIGN KEY (`to_user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE,
    FOREIGN KEY (`design_id`) REFERENCES `postcarddesign`(`id`) ON DELETE CASCADE
    )
    """
)

step(
    """
    INSERT INTO `postcarddesign` (`id`, `path`) VALUES (1, '1.svg'), (2, '2.svg'), (3, '3.svg'), (4, '4.svg');
    """
)
