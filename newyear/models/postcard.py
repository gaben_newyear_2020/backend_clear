from random import choice
from datetime import datetime, timedelta
import peewee
from newyear.models.base import BaseModel
from newyear.models.users import User
from newyear.settings.conf import db_manager


class PostcardGreeting(BaseModel):
    text = peewee.TextField(
        verbose_name="text",
    )

    @classmethod
    async def get_random_greeting(cls):
        rows = await cls.execute(
            cls.select(
                cls.id,
                cls.text
            ).order_by(peewee.fn.Rand()).limit(5)
        )
        return [
            dict(id=x.id, text=x.text)
            for x in rows
        ]


class PostcardDesign(BaseModel):
    path = peewee.CharField(
        verbose_name="path"
    )

    async def get_full_info(self):
        return dict(
            id=self.get_id(),
            url=self.path,
        )


class PostcardData(BaseModel):
    from_user = peewee.ForeignKeyField(
        User,
        on_delete="CASCADE"
    )
    to_user = peewee.ForeignKeyField(
        User,
        on_delete="CASCADE"
    )
    anonymous = peewee.BooleanField(
        verbose_name="anonymous",
        default=False,
        null=False
    )
    text = peewee.TextField(
        verbose_name="text",
    )
    slug = peewee.CharField(
        verbose_name="slug",
        max_length=255,
    )
    design = peewee.ForeignKeyField(
        PostcardDesign,
        on_delete="CASCADE",
    )
    mark_id = peewee.SmallIntegerField(
        default=0,
        verbose_name="mark_id",
    )
    time_send = peewee.DateTimeField(
        verbose_name="time_send",
    )
    is_view = peewee.BooleanField(
        verbose_name="is_view",
        default=False,
    )

    async def get_short_info(self):
        from_user_id = None if self.anonymous else self.from_user.vk_id
        return dict(
            id=self.id,
            from_user_id=from_user_id,
            slug=self.slug,
            is_view=self.is_view,
            time_send=self.time_send,
        )

    async def get_full_info(self):
        from_user_id = None if self.anonymous else self.from_user.vk_id
        return dict(
            from_user_id=from_user_id,
            text=self.text,
            design=await self.design.get_full_info(),
            slug=self.slug,
            is_view=self.is_view,
            mark_id=self.mark_id,
        )

    @classmethod
    async def create_and_generate_slug(cls, **kwargs):
        sumbols = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
        slug = ''.join(choice(sumbols) for i in range(15))
        kwargs['slug'] = slug
        return await cls.create_object(
            **kwargs
        )

    @classmethod
    async def get_no_view_count_postcard_by_user(cls, user) -> dict:
        new_count = await db_manager.count(
            cls.select().where(
                cls.to_user == user,
                cls.is_view == False
            ).order_by(cls.id.desc())
        )
        return new_count

    @classmethod
    async def get_all_by_x_user_id(cls, x_user_id):
        user = await User.get_object(x_user_id)
        array = await db_manager.execute(
            cls.select().where(
                cls.to_user == user,
            ).order_by(cls.id.desc())
        )
        return [await x.get_short_info() for x in array]

    @classmethod
    async def get_or_none_by_slug_and_to_user_id(cls, x_user_id, slug) -> tuple:
        """
        Получение карточки по параметрам
        :param x_user_id:
        :param slug:
        :return:
            0 -> is_error
            1 -> info
        """
        def totocard(cardd):
            if cardd is None or len(cardd) != 1 or cardd[0] is None:
                return None
            return cardd[0]

        user = await User.get_object(x_user_id)
        card = await db_manager.execute(
            cls.select().where(
                cls.to_user == user,
                cls.slug == slug,
            ).order_by(cls.id.desc()).limit(1)
        )
        card = totocard(card)

        if card is None:
            user = await User.get_or_create_by_vk_id(0)
            card = await db_manager.execute(
                cls.select().where(
                    cls.to_user == user,
                    cls.slug == slug,
                ).order_by(cls.id.desc()).limit(1)
            )
            card = totocard(card)
            if card is None:
                return True, dict(details="Slug is incorrect or access denied")
        resp = await card.get_full_info()
        await card.update_object(
            dict(is_view=True)
        )
        return False, resp

    @classmethod
    async def check_allow_send_from_user_to_user(cls, from_user, to_user) -> tuple:
        if to_user.id == 0:
            return False, dict()

        today = datetime.now()
        yesterday = today - timedelta(days=1)
        query = cls.select(
                cls.time_send
            ).where(
                (cls.from_user == from_user) &
                (cls.to_user == to_user) &
                (cls.time_send > yesterday)
            ).order_by(cls.time_send).limit(5)
        print(query.sql())

        envelops = await db_manager.execute(
            cls.select(
                cls.time_send
            ).where(
                (cls.from_user == from_user) &
                (cls.to_user == to_user) &
                (cls.time_send > yesterday)
            ).order_by(cls.time_send).limit(5)
        )
        print("envelops+", len(envelops))

        if len(envelops) >= 5:
            first_time_send = envelops[0].time_send + timedelta(days=1)
            text_time = cls.beautify_time(first_time_send, today)
            return True, dict(
                error="Вы можете отправить сообщение пользователю " + text_time
            )
        return False, dict()

    @staticmethod
    def beautify_time(now_time, time_send):
        def convert_timedelta(duration):
            days, seconds = duration.days, duration.seconds
            hours = days * 24 + seconds // 3600
            minutes = (seconds % 3600) // 60
            seconds = (seconds % 60)
            return hours, minutes, seconds

        h, m, _ = convert_timedelta(
            now_time - time_send
        )

        if h >= 24:
            return "завтра"
        if h > 0:
            str_hours = (None, "час", "2 часа", "3 часа", "4 часа", "5 часов", "6 часов", "7 часов", "8 часов", "9 часов",
             "10 часов", "11 часов", "12 часов", "13 часов", "14 часов", "15 часов", "16 часов", "17 часов", "18 часов",
             "19 часов", "20 часов", "21 час", "22 часа", "23 часа")
            return "через " + str_hours[h]
        if m >= 60:
            return "через час"
        if m == 0:
            return "через некоторое время"
        a, b = m // 10, m % 10
        if a == 1:
            return "через " + str(m) + " минут"
        if b == 0 or 5 <= b <= 9:
            return "через " + str(m) + " минут"
        if b == 1:
            return "через " + str(m) + " минуту"
        return "через " + str(m) + " минуты"
