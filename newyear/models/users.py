from datetime import datetime

from newyear.models.base import BaseModel
import peewee


class User(BaseModel):
    vk_id = peewee.CharField(
        verbose_name="vk_user_id",
        max_length=255
    )
    snow = peewee.IntegerField(
        verbose_name="snow",
        default=5,
    )
    time_registered = peewee.DateTimeField(
        verbose_name="time_registered",
        default=datetime.now()
    )
    last_login = peewee.DateTimeField(
        verbose_name="last_login",
        default=datetime.now(),
        null=True,
    )
    last_time_get_present = peewee.DateTimeField(
        verbose_name="last_time_get_present",
        null=True
    )
    count_consecutive_get_present = peewee.SmallIntegerField(
        default=0,
        verbose_name="count_consecutive_get_present"
    )

    @classmethod
    async def get_or_create_by_vk_id(cls, vk_id):
        user = cls.select().where(
            cls.vk_id == vk_id,
        ).first()
        if user is None:
            user = await cls.create_object(
                vk_id=vk_id,
                time_registered=datetime.now(),
                last_login=datetime.now(),
            )
            return user
        else:
            await user.update_object(
                query=dict(last_login=datetime.now())
            )
            return user

    async def get_full_info(self):
        return dict(
            snow=self.snow,
        )
