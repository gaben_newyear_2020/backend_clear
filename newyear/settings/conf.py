try:
    from .local import *
except ModuleNotFoundError:
    from .base import *

from .mixin_connect import (
    Manager,
    MQDataBase,
    retry_connect
)

database = MQDataBase(
    database=DB_NAME,
    user=DB_USER,
    host=DB_HOST,
    port=int(DB_PORT),
    password=DB_PASSWORD,
    autoconnect=True,
)

db_manager = Manager(database)
