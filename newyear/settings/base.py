import os

ALLOW_URL_CORS = [
    "https://localhost:10888",
    "http://localhost:8080",
]
AMQP_URL = os.getenv("AMQP_URL")

DB_NAME = os.getenv('DB_NAME')
DB_USER = os.getenv('DB_USER')
DB_HOST = os.getenv('DB_HOST')
DB_PORT = os.getenv('DB_PORT')
DB_PASSWORD = os.getenv('DB_PASSWORD')

SITE_DOMAIN = os.getenv('SITE_DOMAIN')
RUN_TYPE = os.getenv('RUN_TYPE')
CLIENT_SECRET = os.getenv('CLIENT_SECRET')
TOKEN_SERVICE = os.getenv('TOKEN_SERVICE')

