import fastapi_jsonrpc as jsonrpc
from redis import Redis
# Создаём приложение fast api и точку входа
app = jsonrpc.API()

api_v1 = jsonrpc.Entrypoint(
    "/jsonrpc",
    errors=jsonrpc.Entrypoint.default_errors,
)

# Connect to REDIS
conf = dict(
    host='redis',
    port=6379,
    db=0
)
redis_client = Redis(**conf)

if __name__ == "__main__":
    def migration():
        print("Start migration in command:")
        from newyear.settings import conf
        DB_USER = conf.DB_USER
        DB_PASSWORD = conf.DB_PASSWORD
        DB_HOST = conf.DB_HOST
        DB_NAME = conf.DB_NAME
        migrate = f"yoyo apply --database mysql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}/{DB_NAME} newyear/migrations"
        print(migrate)

    import uvicorn
    migration()
    uvicorn.run('newyear:app', host="0.0.0.0", port=8000, debug=True)
