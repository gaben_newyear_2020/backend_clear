# API Postcards Service
* version: 1.0.18
* url: https://postcard-newyear.gubanov.site/api/jsonrpc
* Используемая схема параметров - JSON-RPC

Для предотвращения межсайтового RSS - все запросы должны передаваться с домена, на котором установлено сервисное приложение
### Intro
Для написания интерфеса, необходима возможность осуществлять операции через API.
Данное API описывает протокол взаимодействия учетной записи на странице VK (vk connect) с нашим приложением. 

Протокол обмена состоит из HTTP(s) запросов типа POST.
В теле запроса передается JSON cодержимое.
Ответ формируется так же в формате JSON.
Кодировка UTF-8.
Примеры работы представлен в каждом методе с некоторыми комментариями.

### Users Methods
Методы, связанные с профилем пользователя. Методы направленны на конфигурацию аккаунта и пр.
Настраиваемые регионы компаний используются при филтрации городов по выбранным регионам. Настраивание происходит на странице настройки вашей компаний

#### user_open_app
```pydocstring
Функция, которая отдаёт первичные данные для входа в приложение
Срабатывает один раз при входе в приложение
:param: sign - используется при авторизации пользователя (токен от VK)
:param: x_user_id - после авторизации прописывается в функцию и определяет идентификатор пользователя в системе
:return:
    unread: Кол-во не прочитанных открыток для пользователя
```

*Пример запроса 1-2:*
```json
{
    "jsonrpc": "2.0",
    "id": 0,
    "method": "user_open_app",
    "params": {
        "sign": "https://postcard-newyear.gubanov.site/?vk_access_token_settings=menu&vk_app_id=7704043&vk_are_notifications_enabled=0&vk_is_app_user=1&vk_is_favorite=0&vk_language=ru&vk_platform=mobile_web&vk_ref=other&vk_ts=1&vk_user_id=0&sign=XXXXXXXXXXXXXXX-XXXXXXXXXxxXXXXX"
    }
}
```

*Пример ответа 1:*
Успешный ответ
```json
{
    "jsonrpc": "2.0",
    "id": 0,
    "method": "user_open_app",
    "result": {
        "unread": 8
    }
}
```

*Пример ответа 2:*
Не верная подпись, или нет авторизации на платформе VK
```json
{
    "jsonrpc": "2.0",
    "error": {
        "code": 403,
        "message": "Forbidden Error",
        "data": {
            "details": "Sign no valid"
        }
    },
    "id": 0
}
```

*Пример запроса 3:*
```json
{
    "jsonrpc": "2.0",
    "id": 0,
    "method": "user_open_app",
    "params": {}
}
```

*Пример ответа 3:*
Нет подписи для проверки авторизации на платформе
```json
{
    "jsonrpc": "2.0",
    "error": {
        "code": 403,
        "message": "Forbidden Error",
        "data": {
            "details": "No user fields into params"
        }
    },
    "id": 0
}
```

### Postcard Methods
Методы, связанные с открытками.

#### postcard_generate_greetings
```pydocstring
Автодополнение текста
:param text: Текст, который необходимо дополнить
:return: 
    replies - массив из нескольких выриантов дополнения текста
```

*Пример запроса 1:*
```json
{
    "jsonrpc": "2.0",
    "id": 0,
    "method": "postcard_generate_greetings",
    "params": {
        "text": "С новым"
    }
}
```

*Пример ответа 1:*
Успешный ответ
```json
{
    "jsonrpc": "2.0",
    "id": 0,
    "method": "postcard_generate_greetings",
    "result": {
        "replies": [
            " годом",
            " годом!",
            " салатом."
        ]
    }
}
```

#### postcard_create
```pydocstring
Создаёт сущность поздравления
:param mark_id: Идентификатор марки для отображения в конверте
:param sign: 
:param design_id: Идентификатор дизайна открытки для отображения в конверте
:param text: Текст, который будет поздравительным
:param to_vk_id: идентификатор для получателя (внутри платформы VK)
:param x_user_id:
:return: 
    slug - уникальная ссылка для открытки 
        (если при этом, в течение дня от даты отправки 
         получателю от отправителя не приходило более 5 писем)
    error - текст ошибки в интерфейсе (иначе)
```

*Пример запроса 1-3:*
```json
{
    "jsonrpc": "2.0",
    "id": 0,
    "method": "postcard_create",
    "params": {
        "sign": "<vk_sign>",
        "design_id": 1,
        "mark_id": 9,
        "text": "С Новым годом!",
        "to_vk_id": 2
    }
}
```

*Пример ответа 1:*
Успешный ответ
```json
{
    "jsonrpc": "2.0",
    "id": 0,
    "method": "postcard_create",
    "result": {
        "postcard_slug": "cVhdbhDMjdhskdls"
    }
}
```

*Пример ответа 2:*

Идентификатор дизайна открытки не является валидным
```json
{
    "jsonrpc": "2.0",
    "error": {
        "code": 500,
        "message": "Field is not valid'",
        "data": {
            "details": "design_id"
        }
    },
    "id": 0
}
```

*Пример ответа 3:*

Ограничение лимитом сообщений до 5 писем кадлому пользователю в день
```json
{
    "jsonrpc": "2.0",
    "id": 0,
    "method": "postcard_create",
    "result": {
        "error": "Вы можете отправить сообщение пользователю через 15 минут"
    }
}
```

#### postcard_get
```pydocstring
Получение открытки по slug
:param slug: Доступ к открытке
:param sign:
:param x_user_id:
:return:
    from_user_id - идентификатор пользователя на платформе VK
    from_user - профиль пользователя:
        first_name,
        last_name,
        photo_200,
        ...
    text - текст поздравления
    design:
        id - уникальный идентификатор дизайна в системе
        url - ссылка на отображение картинки дизайна
    slug - уникальная строка для получения поздравления
    is_view - просмотрена ли открытка ранее
    mark_id - число, которое является идентификатором марки для отображения на конверте
```

*Пример запроса 1-2:*
```json
{
    "jsonrpc": "2.0",
    "id": 0,
    "method": "postcard_get",
    "result": {
        "sign": "<VK sign>",
        "slug": "VW9Esm7tUDXhE0l"
    }
}
```

*Пример ответа 1:*

Открытка в базе не найдена, или принадлежит другому человеку
```json
{
    "jsonrpc": "2.0",
    "error": {
        "code": 500,
        "message": "Field is not valid'",
        "data": {
            "details": "Slug is incorrect or access denied"
        }
    },
    "id": 0
}
```

*Пример ответа 2:*

Успешный ответ
```json
{
    "jsonrpc": "2.0",
    "id": 0,
    "result": {
        "from_user_id": "1",
        "text": "С релизом! И это без защиты от подделок!",
        "design": {
            "id": 4,
            "url": "4.svg"
        },
        "slug": "VW9Esm7tUDXhE0l",
        "is_view": true,
        "mark_id": 5,
        "from_user": {
            "first_name": "Павел",
            "id": 1,
            "last_name": "Дуров",
            "can_access_closed": true,
            "is_closed": false,
            "photo_200": "https://sun9-20.userapi.com/impf/c836333/v836333001/31189/8To0r3d-6iQ.jpg?size=200x0&quality=96&crop=1001,1015,567,567&sign=e281202da72edadcb1059ec82eebfd42&c_uniq_tag=lHdiYnp8XRUQ2mIL6l08xjmxBXi6_nsQoI2PaK-D-dg&ava=1"
        }
    }
}
```

#### postcard_my
```pydocstring
Получить все открытки, принадлежащие человеку
:param sign:
:param x_user_id:
:return:
    count - кол-во открыток
    postcards - вся карткая информация обо всех открыток, которые прислали текущему пользователю
```

*Пример запроса 1:*
```json
{
    "jsonrpc": "2.0",
    "id": 0,
    "method": "postcard_my",
    "params": {
        "sign": "vk_sign"
    }
}
```

*Пример ответа 1:*
```json
{
    "jsonrpc": "2.0",
    "id": 0,
    "result": {
        "count": 2,
        "postcards": [
            {
                "id": 38,
                "from_user_id": "412904874",
                "slug": "OtPgAmWsD6YK8sb",
                "is_view": true,
                "time_send": "2020-12-28T10:33:07",
                "from_user": {
                    "first_name": "Павел",
                    "id": 412904874,
                    "last_name": "Милков",
                    "can_access_closed": true,
                    "is_closed": false,
                    "photo_200": "https://sun6-23.userapi.com/impg/T1Q9kXx6NcYHXZitS5Nxqz5gGr4HZCprxGwbvg/ROkQDpCfBFM.jpg?size=200x0&quality=96&crop=0,249,1658,1658&sign=01eb5cf139c2562366960239eb8192a0&c_uniq_tag=f02BT7F3H_pXvjueHLVU_wKCs7zmTSolbqoeqSQfub4&ava=1"
                }
            },
            {
                "id": 4,
                "from_user_id": "1",
                "slug": "VW9Esm7tUDXhE0l",
                "is_view": true,
                "time_send": "2020-12-20T07:31:32",
                "from_user": {
                    "first_name": "Павел",
                    "id": 1,
                    "last_name": "Дуров",
                    "can_access_closed": true,
                    "is_closed": false,
                    "photo_200": "https://sun9-20.userapi.com/impf/c836333/v836333001/31189/8To0r3d-6iQ.jpg?size=200x0&quality=96&crop=1001,1015,567,567&sign=e281202da72edadcb1059ec82eebfd42&c_uniq_tag=lHdiYnp8XRUQ2mIL6l08xjmxBXi6_nsQoI2PaK-D-dg&ava=1"
                }
            },
        ]
    }
}
```
